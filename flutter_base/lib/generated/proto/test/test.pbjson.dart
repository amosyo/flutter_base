///
//  Generated code. Do not modify.
//  source: test/test.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const PbLevel$json = const {
  '1': 'PbLevel',
  '2': const [
    const {'1': 'NORMAL', '2': 0},
    const {'1': 'VIP_1', '2': 1},
  ],
};

const PbGender$json = const {
  '1': 'PbGender',
  '2': const [
    const {'1': 'UN_KNOWN', '2': 0},
    const {'1': 'MALE', '2': 1},
    const {'1': 'FEMALE', '2': 2},
  ],
};

const PbUser$json = const {
  '1': 'PbUser',
  '2': const [
    const {'1': 'uid', '3': 1, '4': 1, '5': 4, '10': 'uid'},
    const {'1': 'nick', '3': 2, '4': 1, '5': 9, '10': 'nick'},
    const {'1': 'gender', '3': 3, '4': 1, '5': 14, '6': '.com.base.user.PbGender', '10': 'gender'},
    const {'1': 'level', '3': 4, '4': 1, '5': 5, '10': 'level'},
  ],
};

