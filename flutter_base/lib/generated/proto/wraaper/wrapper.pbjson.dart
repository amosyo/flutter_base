///
//  Generated code. Do not modify.
//  source: wraaper/wrapper.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const PbResponse$json = const {
  '1': 'PbResponse',
  '2': const [
    const {'1': 'code', '3': 1, '4': 1, '5': 5, '10': 'code'},
    const {'1': 'data', '3': 2, '4': 1, '5': 12, '10': 'data'},
    const {'1': 'reason', '3': 3, '4': 1, '5': 9, '10': 'reason'},
    const {'1': 'toast', '3': 4, '4': 1, '5': 9, '10': 'toast'},
    const {'1': 'dialog', '3': 5, '4': 1, '5': 9, '10': 'dialog'},
  ],
};

