///
//  Generated code. Do not modify.
//  source: wraaper/wrapper.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class PbResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('PbResponse', package: const $pb.PackageName('com.base.wrapper'), createEmptyInstance: create)
    ..a<$core.int>(1, 'code', $pb.PbFieldType.O3)
    ..a<$core.List<$core.int>>(2, 'data', $pb.PbFieldType.OY)
    ..aOS(3, 'reason')
    ..aOS(4, 'toast')
    ..aOS(5, 'dialog')
    ..hasRequiredFields = false
  ;

  PbResponse._() : super();
  factory PbResponse() => create();
  factory PbResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PbResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  PbResponse clone() => PbResponse()..mergeFromMessage(this);
  PbResponse copyWith(void Function(PbResponse) updates) => super.copyWith((message) => updates(message as PbResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PbResponse create() => PbResponse._();
  PbResponse createEmptyInstance() => create();
  static $pb.PbList<PbResponse> createRepeated() => $pb.PbList<PbResponse>();
  @$core.pragma('dart2js:noInline')
  static PbResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PbResponse>(create);
  static PbResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get code => $_getIZ(0);
  @$pb.TagNumber(1)
  set code($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCode() => $_has(0);
  @$pb.TagNumber(1)
  void clearCode() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get data => $_getN(1);
  @$pb.TagNumber(2)
  set data($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasData() => $_has(1);
  @$pb.TagNumber(2)
  void clearData() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get reason => $_getSZ(2);
  @$pb.TagNumber(3)
  set reason($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasReason() => $_has(2);
  @$pb.TagNumber(3)
  void clearReason() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get toast => $_getSZ(3);
  @$pb.TagNumber(4)
  set toast($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasToast() => $_has(3);
  @$pb.TagNumber(4)
  void clearToast() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get dialog => $_getSZ(4);
  @$pb.TagNumber(5)
  set dialog($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasDialog() => $_has(4);
  @$pb.TagNumber(5)
  void clearDialog() => clearField(5);
}

