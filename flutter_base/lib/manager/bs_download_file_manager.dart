import 'dart:convert';
import 'dart:io';

import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:install_plugin/install_plugin.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';

///app更新管理类
///by gurs95
class BsDownloadFileManager {
  factory BsDownloadFileManager() => _getInstance();

  static BsDownloadFileManager get instance => _getInstance();
  static BsDownloadFileManager _instance;

  BsDownloadFileManager._internal() {
    // 初始化
  }

  static BsDownloadFileManager _getInstance() {
    if (_instance == null) {
      _instance = BsDownloadFileManager._internal();
    }
    return _instance;
  }

  ///下载app
  Future<void> onDownloadApkFile(
    String apkDownloadUrl, {
    String apkFilePath,
    DownloadProgressCallback downloadProgressCallback,
    VerifyMD5Callback verifyMD5Callback,
    DownloadSucCallback downloadSucCallback,
    DownloadFaiCallback downloadFaiCallback,
  }) async {
    if (apkFilePath?.isNotEmpty != true) {
      Directory _directory = await getExternalStorageDirectory();
      apkFilePath = '${_directory.path}/apk/debug.apk';
    }

    File _apkFile = File(apkFilePath);
    bool _exists = await _apkFile.exists();
    if (_exists == true) {
      var _fileBytes = await _apkFile.readAsBytes();
      var _digest = md5.convert(_fileBytes);
      String _md5Key = '${_digest.toString()}';
      print('-已存在apk的md5-->>>$_md5Key');
      bool _hasMd5 = verifyMD5Callback?.call(_md5Key);
      if (_hasMd5 == true) {
        downloadProgressCallback?.call(100, 100);
        downloadSucCallback?.call(apkFilePath);
        return;
      }
      await _apkFile.delete();
      print('-删除旧版本-->>>$apkFilePath');
    }
    return await onDownloadFile(
      downloadUrl: apkDownloadUrl,
      saveFilePath: apkFilePath,
      downloadProgressCallback: downloadProgressCallback,
      verifyMD5Callback: verifyMD5Callback,
      downloadSucCallback: downloadSucCallback,
      downloadFaiCallback: downloadFaiCallback,
    );
  }

  ///下载文件
  Future<void> onDownloadFile({
    String downloadUrl,
    String saveFilePath,
    ProgressCallback onReceiveProgress,
    VerifyMD5Callback verifyMD5Callback,
    DownloadProgressCallback downloadProgressCallback,
    DownloadSucCallback downloadSucCallback,
    DownloadFaiCallback downloadFaiCallback,
  }) async {
    try {
      Dio dio = Dio();
      //设置连接超时时间
      // dio.options.connectTimeout = 60 * 1000;
      print('-下载地址-->>>$downloadUrl');
      print('-保存地址-->>>$saveFilePath');
      Response response = await dio.download(
        downloadUrl,
        saveFilePath,
        onReceiveProgress: onReceiveProgress ??
            (int count, int total) {
              if (total != -1) {
                downloadProgressCallback?.call(count, total);
                var _progress = (count / total * 100).toStringAsFixed(0) + "%";
                print('--下载回调-->>>$_progress');
              }
            },
      );
      print('--下载response-->>>${response?.statusCode}');
      print('--下载response-->>>${response?.toString()}');
      if (response?.statusCode == 200) {
        downloadSucCallback?.call(saveFilePath);
      } else {
        downloadFaiCallback?.call('下载失败(0)');
      }
    } catch (e) {
      String errorCode = '未知错误(-1)';
      if (e is DioError) {
        errorCode = e?.message;
      }
      downloadFaiCallback?.call(errorCode);
    }
  }

  ///安装apk文件
  Future<void> onInstallApk(String apkFilePath) async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String packageName = packageInfo.packageName;
    print('包名-->>$packageName');
    InstallPlugin.installApk(apkFilePath, packageName).then((result) {
      print('安装结果--install apk $result');
    }).catchError((error) {
      print('安装结果--install apk error: $error');
    });
  }

  ///打开[ios] appStore
  void onGotoAppStore(String url) {
    if (url?.isNotEmpty == true) {
      InstallPlugin.gotoAppStore(url);
    } else {
      print('-url->>isEmpty or null');
    }
  }
}

typedef DownloadProgressCallback = void Function(int count, int total);
typedef VerifyMD5Callback = bool Function(String md5);
typedef DownloadSucCallback = void Function(String apkFilePath);
typedef DownloadFaiCallback = void Function(String errorMsg);
