import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';

///网络连接情况
///by grus95
///
/// 初始化
///
///  BsConnectivityManager.instance.init((result) {
///       print('网络监听回调-->>$result');
///       VMBean.provider<ConnectivityResult>(context).updateSelf(result);
///       eventBusSend(EbCode.network_update, data: result);
///     });
///
/// 回收
///
/// BsConnectivityManager.instance.dispose();
///
class BsConnectivityManager {
  factory BsConnectivityManager() => _getInstance();

  static BsConnectivityManager get instance => _getInstance();
  static BsConnectivityManager _instance;

  BsConnectivityManager._internal() {
    // 初始化
  }

  static BsConnectivityManager _getInstance() {
    if (_instance == null) {
      _instance = BsConnectivityManager._internal();
    }
    return _instance;
  }

  ConnectivityResult _connectivityResult;
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  ///初始化监听
  void init(
    ConnectivityStateCall stateCall, {
    String pingHost,
  }) {
    _initConnectivity(stateCall);
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen(
      (result) {
        if (_connectivityResult != result) {
          _initConnectivity(stateCall, pingHost: pingHost);
        }
      },
    );
  }

  ///回收监听
  void dispose() {
    _connectivitySubscription.cancel();
  }

  /// Platform messages are asynchronous, so we initialize in an async method.
  Future<void> _initConnectivity(
    ConnectivityStateCall stateCall, {
    String pingHost,
  }) async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      _connectivityResult = await _connectivity.checkConnectivity();
      if ([
            ConnectivityResult.mobile,
            ConnectivityResult.wifi,
          ].contains(_connectivityResult) &&
          pingHost != null &&
          pingHost.isNotEmpty) {
        bool _hasNetWork = await _checkInternet(pingHost: pingHost);
        if (_hasNetWork != true) {
          _connectivityResult = ConnectivityResult.none;
        }
      }
    } on PlatformException catch (e) {
      print(e.toString());
      _connectivityResult = ConnectivityResult.none;
    }
    stateCall?.call(_connectivityResult ?? ConnectivityResult.none);
  }

  ///检查网络是否能正常上网
  static Future<bool> _checkInternet({String pingHost}) async {
    bool connectionStatus;
    try {
      String host = pingHost ?? "baidu.com";
      final result = await InternetAddress.lookup(host);
      print("检查网络是否能正常上网result-- $result");
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connectionStatus = true;
        print("检查网络是否能正常上网connected $connectionStatus");
      }
    } on SocketException catch (_) {
      connectionStatus = false;
      print("检查网络是否能正常上网not connected $connectionStatus");
    }
    return connectionStatus;
  }
}

///网络连接更新回调
typedef ConnectivityStateCall = void Function(ConnectivityResult result);
