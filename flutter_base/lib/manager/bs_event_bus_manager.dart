import 'dart:async';

import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';

///混入widget的eventBus
///by grus95
///
/// 使用
/// 建议是依附在有生命周期的widget类上，如：
/// class TestView extends StatefulWidget with BsEventBusMixin<EbCode>{
///
///   @override
///   void initState() {
///     super.initState();
///     initEventBus(context);
///   }
///
///   @override
///   void dispose() {
///     disposeEventBus();
///     super.dispose();
///   }
///
///   //发送
///   void testSend(){
///      eventBusSend();
///   }
///
///   @override
///   ConsumerListener<EbCode> eventBusConsumerListener() {
///     return (BuildContext context, EbBean<EbCode, dynamic> ebBean) {
///       //接受
///   }}
///
/// }
mixin BsEventBusMixin<EC> {
  StreamSubscription streamSubscription;

  ///EventBus初始化
  void initEventBus(BuildContext context) {
    ConsumerListener<EC> _listener = eventBusConsumerListener();
    if (_listener != null) {
      streamSubscription = BsEventBusManager.instance.receive<EC>(
        context,
        listener: _listener,
      );
    }
  }

  ///封装了下发送eventBus
  void eventBusSend(EC ebCode, {dynamic data}) {
    BsEventBusManager.instance.send<EC>(ebCode, data: data);
  }

  ///子类监听使用eventBus
  ///使用
  //  @override
  //    ConsumerListener<EbCode> eventBusConsumerListener() {
  //     return (BuildContext context, EbBean<EbCode, dynamic> ebBean) {
  //       var _code = bean?.code;
  //       var _data = bean?.data;
  //       switch (_code) {
  //         default:
  //           break;
  //       }
  //     };
  //   }
  ConsumerListener<EC> eventBusConsumerListener() => null;

  ///EventBus回收
  void disposeEventBus() {
    streamSubscription?.cancel();
  }
}

///事件流管理者
///by gurs95
///
///
///使用
///参考[BsEventBusMixin]
///实现需要新建类似[TestCode]的枚举类，用于确认事件类型
///最终合并为 => BsEventBusMixin<TestCode>
class BsEventBusManager {
  factory BsEventBusManager() => _getInstance();

  static BsEventBusManager get instance => _getInstance();
  static BsEventBusManager _instance;

  BsEventBusManager._internal() {
    // 初始化
    if (_eventBus == null) {
      _eventBus = EventBus();
    }
  }

  static BsEventBusManager _getInstance() {
    if (_instance == null) {
      _instance = BsEventBusManager._internal();
    }
    return _instance;
  }

  EventBus _eventBus;

  EventBus get eventBus => instance._eventBus;

  ///初始化
  ///内容限制初始化的逻辑
  StreamSubscription<T> _initEventBusListen<T>(
    void onData(T event), {
    Function onError,
    void onDone(),
    bool cancelOnError,
  }) {
    return _eventBus.on<T>().listen(
          onData,
          onError: onError,
          onDone: onDone,
          cancelOnError: cancelOnError,
        );
  }

  ///发送数据
  void sendEbBean<EC>(EbBean<EC, dynamic> ebBean) {
    _eventBus?.fire(ebBean);
  }

  ///发送数据
  ///[EbBean]
  void send<EC>(
    EC ebCode, {
    dynamic data,
  }) {
    sendEbBean<EC>(EbBean<EC, dynamic>(ebCode, data: data));
  }

  ///接收数据
  StreamSubscription receive<EC>(
    BuildContext context, {
    @required ConsumerListener<EC> listener,
    Function onError,
    void onDone(),
    bool cancelOnError,
  }) {
    return _initEventBusListen<EbBean<EC, dynamic>>(
      (event) => listener?.call(context, event),
      onError: onError,
      onDone: onDone,
      cancelOnError: cancelOnError,
    );
  }
}

///事件接收回调
typedef void ConsumerListener<EC>(
  BuildContext context,
  EbBean<EC, dynamic> ebBean,
);

///eventBus的传递bean
class EbBean<EC, ED> {
  EC code;
  ED data;

  EbBean(
    this.code, {
    this.data,
  });

  // 命名构造函数
  EbBean.empty();
}

///eventBus的传递code
enum TestCode {
  ///未知情况
  unknown,
}
